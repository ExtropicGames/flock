package com.extropicstudios.flock;

public enum UIAction {
	    NO_ACTION,
	    SELECT,
	    EXAMINE,
	    START_MOVE,
	    END_MOVE,
	    INVENTORY,
	    END_TURN,
	    START_ATTACK,
	    END_ATTACK,
	    FINISH
	}

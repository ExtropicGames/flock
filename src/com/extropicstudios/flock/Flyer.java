package com.extropicstudios.flock;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Flyer extends Entity {

	protected static double rotateMultiplier = 0.001f;
	protected static double speedMultiplier = 0.2f;
	
	private Image image;
	
	public Flyer(float x, float y, float rotation) {
		setX(x);
		setY(y);
		setRotation(rotation);
		try {
			image = new Image("data/ship.png");
		} catch (SlickException e) {
			System.out.println("Couldn't load ship image. Make sure it is in the location data/ship.png.");
		}
	}
	
	public String toString() {
		return String.valueOf(rotation);
	}
	
	@Override
	public void render(GameContainer gc, Graphics g) {
		image.setRotation((float) Math.toDegrees(rotation));
		image.draw((float) x(), (float) y());
	}

	@Override
	public void update(GameContainer gc, int delta) {
		
		// Not really necessary, but make sure the rotation level always varies between 0 and 2*pi
		rotation = Trig.minimizeAngle(rotation);
		
		// Move the ship along its rotation vector
		moveForward(delta);
		
		// If the ship hits a screen edge, make it bounce off.
		// TODO: The bounce code could probably use some work.
		// See http://stackoverflow.com/questions/573084/how-to-calculate-bounce-angle
		if (x() + image.getWidth() > gc.getWidth()) {
			if (Trig.isInQuad4(rotation))
				rotation = Trig.moveToQuad3(rotation);
			else if (Trig.isInQuad1(rotation))
				rotation = Trig.moveToQuad2(rotation);
			setX(gc.getWidth() - image.getWidth());
		}
		if (x() < 0) {
			if (Trig.isInQuad2(rotation))
				rotation = Trig.moveToQuad1(rotation);
			else if (Trig.isInQuad3(rotation))
				rotation = Trig.moveToQuad4(rotation);
			setX(0);
		}
		if (y() + image.getHeight() > gc.getHeight()) {
			if (Trig.isInQuad2(rotation))
				rotation = Trig.moveToQuad3(rotation);
			else if (Trig.isInQuad1(rotation))
				rotation = Trig.moveToQuad4(rotation);
			setY(gc.getHeight() - image.getHeight());
		}
		if (y() < 0) {
			if (Trig.isInQuad4(rotation))
				rotation = Trig.moveToQuad1(rotation);
			else if (Trig.isInQuad3(rotation))
				rotation = Trig.moveToQuad2(rotation);
			setY(0);
		}
	}
}

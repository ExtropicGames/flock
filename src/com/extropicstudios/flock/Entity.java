package com.extropicstudios.flock;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public abstract class Entity {

	protected static final double rotationSpeed = 0.001f;
	protected static final double movementSpeed = 0.2f;
	
	protected double x;
	protected double y;
	protected double rotation;
	
	public abstract void render(GameContainer gc, Graphics g);
	
	public abstract void update(GameContainer gc, int delta);
	
	public void setX(double x) { this.x = x; }
	public void setY(double y) { this.y = y; }
	public void setRotation(double rotation) { this.rotation = rotation; }
	
	public double x() {return x;}
	public double y() {return y;}
	public double rotation() {return rotation;}
	
	public void rotateClockwise(int delta) {
		rotation -=  (rotationSpeed * delta);
	}
	
	public void rotateCounterClockwise(int delta) {
		rotation += (rotationSpeed * delta);
	}
	
	public void moveForward(int delta) {
		x += Math.cos(rotation) * (movementSpeed * delta);
		y += Math.sin(rotation) * (movementSpeed * delta);
	}
}

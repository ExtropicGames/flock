package com.extropicstudios.flock;

import java.util.ArrayList;
import java.util.List;

public class ScriptString {
	
	private List<String> strings;
	private List<Object> objects;
	
	public ScriptString(String theString, List<Object> objects) {
		this.objects = objects;
		
		// TODO: do this a better way than splitting, like using indexOf.
		String[] fragments = theString.split("%v");
		
		strings = new ArrayList<String>();
		for (String s : fragments) {
			strings.add(s);
		}
		if (objects.size() > strings.size())
			objects = objects.subList(0, strings.size());
	}
	
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		int i = 0;
		for (Object o : objects) {
			result.append(strings.get(i));
			result.append(o);
			i++;
		}
		if (strings.size() > i) {
			result.append(strings.get(i));
		}
		return result.toString();
	}
}

package com.extropicstudios.flock;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;

public class Player extends Flyer {

	public Player(float x, float y, float rotation) {
		super(x, y, rotation);
	}
	
	@Override
	public void update(GameContainer gc, int delta) {
		
		Input input = gc.getInput();
		
		// Process key input to rotate the ship.
		if (input.isKeyDown(Input.KEY_LEFT))
			rotateClockwise(delta);
		if (input.isKeyDown(Input.KEY_RIGHT))
			rotateCounterClockwise(delta);
		
		super.update(gc, delta);
	}

}

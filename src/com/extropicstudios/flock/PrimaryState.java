package com.extropicstudios.flock;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class PrimaryState extends BasicGameState {

	int stateID = -1;
	
	private Rectangle background;
	
	private Monitor monitor;
	
	private List<Entity> entities;
	
	public PrimaryState(int stateID) {
		this.stateID = stateID;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		gc.setVSync(true);
		
		background = new Rectangle(0,0,gc.getScreenWidth(),gc.getScreenHeight());
		entities = new ArrayList<Entity>();
		entities.add(new Player(100,100,0));
		//monitor = new Monitor("Angle: ", entities.get(0), gc.getScreenWidth(), gc.getScreenHeight());
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.setColor(Color.white);
		g.fill(background);
		
		for (Entity e : entities) {
			e.render(gc, g);
		}
		
		if (monitor != null)
			monitor.render(g, 0, 0);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		for (Entity e : entities) {
			e.update(gc, delta);
		}
		
		if (monitor != null)
			monitor.update();
	}

	@Override
	public int getID() {
		return stateID;
	}

}

package com.extropicstudios.flock;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;

public class Flocker extends Flyer {

	private static final double CLOSE_DISTANCE = 12;
	private static final double MIDRANGE_DISTANCE = 24;
	
	List<Entity> flockList;
	
	List<Entity> obstacleList;
	
	public Flocker(float x, float y, float rotation) {
		super(x, y, rotation);
		flockList = new ArrayList<Entity>();
		obstacleList = new ArrayList<Entity>();
	}
	
	public void addToFlockList(Entity flocker) {
		flockList.add(flocker);
	}
	
	public void addToObstacleList(Entity obstacle) {
		obstacleList.add(obstacle);
	}
	
	@Override
	public void update(GameContainer gc, int delta) {
		// When deciding to move, we can do one of three things.
		// Rotate clockwise
		// Rotate counter-clockwise
		// Stay on the same path
		
		List<Entity> midrangeFlock = getMidrangeFlockMembers();
		
		// Steps to take
		// Avoid close neighbors
		
		// Move towards center of midrange flock
		
		// Stay aligned with midrange neighbors
		double avgHeading = getAverageAngleOfFlock(midrangeFlock);
		if (avgHeading > rotation)
		
		super.update(gc, delta);
	}
	
	private double getAverageAngleOfFlock(List<Entity> flock) {
		double total = 0;
		
		for (Entity e : flock) {
			total += Trig.minimizeAngle(e.rotation());
		}
		
		return total / flock.size();
	}
	
	private List<Entity> getCloseFlockMembers() {
		List<Entity> closeFlockers = new ArrayList<Entity>();
		
		for (Entity e : flockList) {
			if (Trig.distanceBetweenPoints(this.x(), this.y(), e.x(), e.y()) <= CLOSE_DISTANCE)
				closeFlockers.add(e);
		}
		return closeFlockers;
	}
	
	private List<Entity> getMidrangeFlockMembers() {
		List<Entity> midrangeFlockers = new ArrayList<Entity>();
		
		for (Entity e : flockList) {
			double distance = Trig.distanceBetweenPoints(this.x(), this.y(), e.x(), e.y());
			if (distance <= MIDRANGE_DISTANCE && distance > CLOSE_DISTANCE)
				midrangeFlockers.add(e);
		}
		return midrangeFlockers;
	}

}

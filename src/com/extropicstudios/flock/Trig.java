package com.extropicstudios.flock;

public class Trig {
	private static double PI = Math.PI;
	
	public static double distanceBetweenPoints(double x1, double y1, double x2, double y2) {
		double xDist = Math.abs(x1 - x2);
		double yDist = Math.abs(y1 - y2);
		
		double hypotenuse = Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));
		
		return hypotenuse;
	}
	
	public static double minimizeAngle(double angle) {
		while (angle < 0)
			angle = angle + (2 * PI);
		while (angle >  2 * PI)
			angle = angle - (2 * Math.PI);
		return angle;
	}
	
	public static boolean isInQuad1(double angle) {
		angle = minimizeAngle(angle);
		return (angle < (PI * 0.5));
	}
	
	public static boolean isInQuad2(double angle) {
		angle = minimizeAngle(angle);
		return (angle > (PI * 0.5) && angle < (PI * 1));
	}
	
	public static boolean isInQuad3(double angle) {
		angle = minimizeAngle(angle);
		return (angle > (PI * 1) && angle < (PI * 1.5));
	}
	
	public static boolean isInQuad4(double angle) {
		angle = minimizeAngle(angle);
		return (angle > (PI * 1.5));
	}
	
	public static double moveToQuad1(double angle) {
		angle = minimizeAngle(angle);
		if (isInQuad2(angle))
			return angle - (0.5 * PI);
		if (isInQuad3(angle))
			return angle - (1 * PI);
		if (isInQuad4(angle))
			return angle - (1.5 * PI);
		if (angle == (PI * 1))
			return 0;
		if (angle == PI * 1.5)
			return PI * 0.5;
		
		return angle;
	}
	
	public static double moveToQuad2(double angle) {
		angle = minimizeAngle(angle);
		if (isInQuad1(angle))
			return angle + (0.5 * PI);
		if (isInQuad3(angle))
			return angle - (0.5 * PI);
		if (isInQuad4(angle))
			return angle - (1 * PI);
		if (angle == 0)
			return (PI * 1);
		if (angle == PI * 1.5)
			return PI * 0.5;
		
		return angle;
	}
	
	public static double moveToQuad3(double angle) {
		angle = minimizeAngle(angle);
		if (isInQuad1(angle))
			return angle + (1 * PI);
		if (isInQuad2(angle))
			return angle + (0.5 * PI);
		if (isInQuad4(angle))
			return angle - (0.5 * PI);
		if (angle == 0)
			return (PI * 1);
		if (angle == PI * 0.5)
			return PI * 1.5;
		
		return angle;
	}
	
	public static double moveToQuad4(double angle) {
		angle = minimizeAngle(angle);
		if (isInQuad1(angle))
			return angle + (1.5 * PI);
		if (isInQuad2(angle))
			return angle + (1 * PI);
		if (isInQuad3(angle))
			return angle + (0.5 * PI);
		if (angle == (PI * 1))
			return 0;
		if (angle == PI * 0.5)
			return PI * 1.5;
		
		return angle;
	}

}

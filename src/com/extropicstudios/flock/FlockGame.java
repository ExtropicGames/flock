package com.extropicstudios.flock;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class FlockGame extends StateBasedGame {

	public static final int PRIMARY_STATE = 0;
	
	public FlockGame() {
		super("Flock");
		
		this.addState(new PrimaryState(PRIMARY_STATE));
		this.enterState(PRIMARY_STATE);
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.getState(PRIMARY_STATE).init(gc, this);
	}
	
	public static void main(String[] args) throws SlickException {
		AppGameContainer app = new AppGameContainer(new FlockGame());
		
		app.setDisplayMode(800, 600, false);
		app.start();
	}

}

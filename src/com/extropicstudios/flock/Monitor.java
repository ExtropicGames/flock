package com.extropicstudios.flock;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

import com.extropicstudios.flock.UIAction;

public class Monitor {
	
	private static final String FONT = "data/dlxfont.ttf";
	
	public enum Type {NORMAL, RISING, MARQUEE_LEFT, MARQUEE_RIGHT}

	private UnicodeFont font;
	
	private Type type;
	
	private float x;
	private float y;
	
	private float parentWidth;
	private float parentHeight;
	
	private ScriptString string;
	
	public Monitor(String label, Object object, float parentWidth, float parentHeight) {
		this(label, object, parentWidth, parentHeight, Type.NORMAL);
	}
	
	public Monitor(String label, Object object, float parentWidth, float parentHeight, Type type) {
		
		try {
			font = new UnicodeFont(FONT, 20, false, false);
		} catch (SlickException e) {
			System.out.println("Couldn't find " + FONT);
		}
		
		font.addAsciiGlyphs();
		font.getEffects().add(new ColorEffect(new Color(0, 0, 0)));
		try {
			font.loadGlyphs();
		} catch (SlickException e) {
			System.out.println("Could not load font glyphs.");
		}
		
	    this.parentWidth = parentWidth;
	    this.parentHeight = parentHeight;
	    
		List<Object> list = new LinkedList<Object>();
		list.add(object);
		if (label.indexOf("%v") == -1)
			string = new ScriptString(label + "%v", list);
		else
			string = new ScriptString(label, list);
		
		this.type = type;
	}
	
	public void move(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public UIAction processInput(Input input) {
	    if (y < 0)
	        return UIAction.FINISH;
		return UIAction.NO_ACTION;
	}
	
	public void update() {
		if (type == Type.RISING)
			y--;
		else if (type == Type.MARQUEE_LEFT) {
			x--;
			if (x < 0)
				x = parentWidth;
		}
		else if (type == Type.MARQUEE_RIGHT) {
			x++;
			if (x > parentWidth)
				x = 0;
		}
	}

	public void render(Graphics g, float xOffset, float yOffset) {
		drawString(x + xOffset, y + yOffset, string.toString());
	}
	
	private void drawString(float xOffset, float yOffset, String string) {
	    font.drawString(x + xOffset, y + yOffset, string);
	}
}
